// @flow
import { ShadowNode, vassilify } from "vasille-flow-js";



export class Button extends ShadowNode {
    text = vassilify('');

    $createSignals () {
        this.$defSignal("clicked");
    }

    $createDom () {
        this.$defTag("button", button => {
            button.$defText(this.text);
            button.$listenClick(() => {
                this.$emit("clicked");
            });
        });
    }
}
