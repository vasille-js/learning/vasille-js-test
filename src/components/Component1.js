// @flow
import type { IValue }           from "vasille-flow-js";
import { ShadowNode, vassilify } from "vasille-flow-js";
import { Button }                from "./Button";



export class Component1 extends ShadowNode {
    count : IValue<number> = vassilify(0);
    text : IValue<string> = vassilify('');

    $created () {
        this.text.$ = "Press me now!";
    }

    $createDom () {
        this.$defTag("p", p => {
            p.$defText("You have pressed ");
            p.$defText(this.count);
            p.$defText(" times");
            // p.$defText(bind(count => {
            //     return `You have pressed ${count} times`;
            // }, this.count));
        });
        this.$defElement(new Button, {text: this.text}, button => {
            button.$on("clicked", () => {
                this.count.$ = this.count.$ + 1;

                if (this.count.$ > 10) {
                    this.text.$ = "It's enough!";
                }
            });
        });
    }
}
