// @flow
import { Component1 }                                              from "./components/Component1";
import { AppNode, ArrayModel, ArrayView, bind, IValue, vassilify } from "vasille-flow-js";



export class App extends AppNode {
    count : IValue<number> = vassilify(0);
    model : IValue<ArrayModel<number>> = vassilify([]);

    constructor () {
        super(document.querySelector("body"), {debug: true});
    }

    $createWatchers () {
        this.$defWatcher((value : number) => {
            if (value > 10) {
                this.model.$.insert(Math.floor(Math.random() * this.model.$.length), value);
            }
        }, this.count);
    }

    $createDom () {
        this.$defElement(new Component1, { count: this.count });
        this.$defIf(bind(count => count > 10, this.count), ifNode => {
            ifNode.$defRepeater(new ArrayView, { model: this.model }, (node, i) => {
                node.$defTag("div", div => {
                    div.$defText(`You clicked more then 10 times! (${i})`);
                });
            });
        });
    }
}
